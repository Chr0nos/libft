[![Build Status](https://travis-ci.org/Chr0nos/libft.svg?branch=master)](https://travis-ci.org/Chr0nos/libft)

Tache               | A faire | En cours | Fait
--------------------|:-------:|:--------:|-----
Partie 1            |         |          | x
Partie 2            |         |          | x
Bonus               |         |          | x
Fonctions perso     |         |     x    |
Optimisation        |         |     x    |
Passage moulinette  |         |          | x
Corrections         |         |          | x
Printf              |         |          | x
Sscanf              |         |     x    |
